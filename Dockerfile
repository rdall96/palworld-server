# PalWorld Docker
# Author(s): Ricky Dall'Armellina (rdall96@gmail.com), Dominic Triano (d.triano0100@gmail.com)
#
# SteamCMD install and usage guide: https://developer.valvesoftware.com/wiki/SteamCMD
# Palworld server setup guide: https://pimylifeup.com/ubuntu-palworld-dedicated-server/

#
# Stage 0: Update and Install
# Desc: Update OS, Install Steam, and Install Palworld
# 
FROM ubuntu:jammy AS game-install

# Install updates and
# Add 32-bit architecture to the package manager, required to install SteamCMD
RUN dpkg --add-architecture i386 \
    && apt-get -y update \
    && apt-get -y upgrade

# Install dependencies
RUN apt-get -y install lib32gcc-s1 curl

# Install SteamCMD to run the steam client and install the game
WORKDIR /steamcmd
RUN curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar -xz
RUN ./steamcmd.sh +force_install_dir $HOME/steamworks \
    +login anonymous \
    +app_update 1007 \
    +quit

# Download Palworld
RUN ./steamcmd.sh +force_install_dir $HOME/palworld \
    +login anonymous \
    +app_update 2394010 \
    validate \
    +quit
# Remove unused steam files from the game install
RUN rm -rf \
    $HOME/palworld/steamapps \
    $HOME/palworld/steamclient.so

#
# Stage 1: Copy Essential Files
# Desc: This stage will copy over only essential game files into the final image in an attempt to save space
#
FROM ubuntu:jammy AS release

# Copy dependencies
COPY --from=game-install /etc/ssl/certs/ca-certificates.crt     /etc/ssl/certs/

# Create a user account to run the server
RUN useradd -m steam
USER steam

# Copy over game files
COPY --from=game-install --chown=steam:steam /root/palworld      /palworld
# Copy the steamclient.so library the game needs to run
COPY --from=game-install --chown=steam:steam /root/steamworks/linux64/steamclient.so    /home/steam/.steam/sdk64/

# Create saves directory
RUN mkdir -p /palworld/Pal/Saved

# Setup entry-point to run the server
WORKDIR /palworld
COPY --chown=steam:steam --chmod=755 scripts/run.sh .
VOLUME [ "/palworld/Pal/Saved" ]
EXPOSE 8211/tcp
EXPOSE 8211/udp
ENTRYPOINT [ "./run.sh" ]
