# Palworld Server

A painless Palworld private server setup for Docker!

# Usage
Start the container:

        docker run -d \
            --name palworld-server \
            -p 8211:8211/tcp \
            -p 8211:8211/udp \
            --mount type=bind,source="./Saved",target=/palworld/Pal/Saved \
            -e ServerName="Private PalWorld Server" \
            -e ServerDescription="Hosted from Docker!" \
            -e ServerPassword="Gotta catch them Pals!" \
            --restart=unless-stopped \
            palworld


Help menu:

        docker run --rm palworld --help


### Docker compose

Start:

        docker-compose -f ./docker-compose.yml up -d

Stop:

        docker-compose stop palworld-server

# Configurations:
There are many configurable options when setting up your Palworld server. Below is the translation from variable name to Palword setting/description: ()
TODO: Add capabilities for the following:

| Environment Variable Name             | Default Value             | Description                                                       |
| ------------------------------------: | :------------------------ | :---------------------------------------------------------------- |
| Difficulty                            | None                      | Doesn't seem to matter for dedicated servers.                     |
| DayTimeSpeedRate                      | 1.0                       | Changes how long daytime lasts.                                   |
| NightTimeSpeedRate                    | 1.0                       | Changes how long night lasts.                                     |
| ExpRate                               | 1.0                       | Modifies how much experience you get.                             |
| PalCaptureRate                        | 1.0                       | How likely you are to capture Pals.                               |
| PalSpawnNumRate                       | 1.0                       | How frequently Pals spawn.                                        |
| PalDamageRateAttack                   | 1.0                       | How much damage Pals do.                                          |
| PalDamageRateDefense                  | 1.0                       | How much damage Pals take.                                        |
| PlayerDamageRateAttack                | 1.0                       | How much damage players do.                                       |
| PlayerDamageRateDefense               | 1.0                       | How much damage players take.                                     |
| PlayerStomachDecreaceRate             | 1.0                       | Rate at which hunger decreases.                                   |
| PlayerStaminaDecreaceRate             | 1.0                       | Rate at which stamina decreases.                                  |
| PlayerAutoHPRegeneRate                | 1.0                       | How quickly HP will be automatically restored when hurt.          |
| PlayerAutoHpRegeneRateInSleep         | 1.0                       | How much HP is regenerated while sleeping.                        |
| PalStomachDecreaceRate                | 1.0                       | Rate at which Pal hunger decreases.                               |
| PalStaminaDecreaceRate                | 1.0                       | Rate at which Pal stamina decreases.                              |
| PalAutoHPRegeneRate                   | 1.0                       | How quickly Pal HP will be automatically restored when hurt.      |
| PalAutoHpRegeneRateInSleep            | 1.0                       | How much Pal HP is regenerated while sleeping.                    |
| BuildObjectDamageRate                 | 1.0                       | Damage to objects.                                                |
| BuildObjectDeteriorationDamageRate    | 1.0                       | How much damage built objects will take over time.                |
| CollectionDropRate                    | 1.0                       | Rate at which items are gathered from things like trees or rocks. |
| CollectionObjectHpRate                | 1.0                       | How much HP breakable world items have.                           |
| CollectionObjectRespawnSpeedRate      | 1.0                       | How quickly objects like trees respawn.                            |
| EnemyDropItemRate                     | 1.0                       | Frequency enemies will drop items.                                |
| DeathPenalty                          | All                       | Determines what is dropped on death. Can be set to none.          |
| bEnableAimAssistPad                   | True                      | Lets you enable or disable aim assist.                            |
| bEnableAimAssistKeyboard              | False                     | Lets you enable or disable aim assist.                            |
| DropItemMaxNum                        | 3000                      | Max amount of dropped items allowed at one time.                  |
| BaseCampMaxNum                        | 128                       | Max amount of bases that can be built at once.                    |
| BaseCampWorkerMaxNum                  | 15                        | Max amount of worker Pals that can be at a camp.                  |
| DropItemAliveMaxHours                 | 1.0                       | How long dropped items will stay spawned before disappearing.     |
| bAutoResetGuildNoOnlinePlayers        | False                     | If true, a guild will be disbanded if no players are online.      |
| AutoResetGuildTimeNoOnlinePlayers     | 72.0                      | How quickly an inactive guild will be disbanded.                  |
| GuildPlayerMaxNum                     | 20                        | Max guild player number.                                          |
| PalEggDefaultHatchingTime             | 72.0                      | How long it takes to hatch Pal eggs.                              |
| WorkSpeedRate                         | 1.0                       | How quickly Pals work.                                            |
| CoopPlayerMaxNum                      | 4                         | Max amount of players in a party.                                 |
| ServerPlayerMaxNum                    | 32                        | Max players allowed on a server (caps at 32).                     |
| ServerName                            | "Default Palworld Server" | The server display name. (DUH)                                    |
| ServerDescription                     | ""                        | What is shown when selecting the server in the list.              |
| AdminPassword                         | ""                        | Password used to grant admin access.                              |
| ServerPassword                        | ""                        | Password needed for regular players to join.                      |