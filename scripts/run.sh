#!/bin/bash
# Entry point for the Palworld server

# Globals
SETTINGS_FILE="Pal/Saved/Config/LinuxServer/PalWorldSettings.ini"

# Load the default game settings
declare -A SETTINGS
SETTINGS_DATA=$(cat "DefaultPalWorldSettings.ini" | grep OptionSettings | sed -r 's/OptionSettings=\({1}//g' | sed -r 's/\){1}//g' | sed -r 's/ {1}/_/g')
for option in ${SETTINGS_DATA//,/ }; do
    components=()
    for i in ${option//=/ }; do
        components+=($i)
    done
    SETTINGS[${components[0]}]=${components[1]}
done

printHelp() {
    echo -e "\nCLI Usage Guide\n"
    echo -e "\033[0;37mOPTION \t\t DESCRIPTION\033[0m"
    echo -e "-h|--help \t Show this menu."
    echo -e "-s|--settings \t Print the default server settings and quit."
    echo -e "-v|--verbose \t Print the current settings and other info."
    echo ""
}

printSettings() {
    echo -e "\nServer Settings:"
    for setting in "${!SETTINGS[@]}"; do
        echo -e " - \033[0;36m$setting\033[0m: ${SETTINGS[$setting]}"
    done
    echo ""
}

echo -e "\033[1;37m- PALWORLD SERVER -\033[0m"
VERBOSE=0

# Check args
while [ $# -gt 0 ]; do
    case "$1" in
    -h|--help)
        printHelp && exit 0
        ;;
    -s|--settings)
        printSettings && exit 0
        ;;
    -v|--verbose)
        VERBOSE=1
        ;;
    *)
        echo -e "\033[0;31mUnknown argument \"$1\".\033[0m"
        echo -e "Use \033[1;37m--help\033[0m for more information."
        exit 1
        ;;
    esac
    shift
done

# Load the settings overrides from the environment and prepare to write the settings
SETTINGS_DATA=""
for env_var in "${!SETTINGS[@]}"; do
    # Check if environment variable is set
    if [[ -n "${!env_var}" ]]; then
        value="${!env_var}"
        # If the value contains a space, make sure to add quotes around it
        if [[ $value == *" "* ]]; then
            value="\"$value\""
        fi
        
        # Save the value
        SETTINGS[$env_var]=$value

        # Write the overrides
        if [[ -z $SETTINGS_DATA ]]; then
            SETTINGS_DATA="$env_var=$value"
        else
            SETTINGS_DATA="$SETTINGS_DATA,$env_var=$value"
        fi
    fi
done

# Write the settings
mkdir -p $(dirname $SETTINGS_FILE)
echo "[/Script/Pal.PalGameWorldSettings]" >> $SETTINGS_FILE
echo "OptionSettings=($SETTINGS_DATA)" >> $SETTINGS_FILE

if [[ $VERBOSE -eq 1 ]]; then
    printSettings
fi

# Start the server
echo -e "\033[0;32mStarting the server...\033[0m\n"
./PalServer.sh port=8211
